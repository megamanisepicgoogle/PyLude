import builtins
import copy
import itertools
from abc import ABC, abstractmethod
from inspect import isfunction, getfullargspec
from typing import Callable, List, Any, TypeVar, Generic, Union, Dict, Tuple

# TODO: better ordering

otherwise = True

a = TypeVar("a")
b = TypeVar("b")
c = TypeVar("c")

rmethods = ["__add__", "__sub__", "__mul__", "__matmul__", "__mod__", "__lshift__", "__rshift__", "__truediv__"
                                                                                                  "__floordiv__",
            "__pow__", "__and__", "__or__", "__xor__"]

twomethods = ["__lt__", "__le__", "__eq__", "__ne__", "__gt__", "__ge__", "__getitem__", "__contains__"]

onemethods = ["__neg__", "__pos__", "__invert__"]


def num_args(f):
    isinclass = f.__name__ in rmethods + ["__r" + i[2:] for i in rmethods] + twomethods
    try:
        isusedwithoutself = f.__self__ is not None
    except AttributeError:
        isusedwithoutself = False
    if isinclass and isusedwithoutself:
        return 1
    elif isinclass:
        return 2

    if isfunction(f):
        return len(getfullargspec(f).args)
    else:
        spec = f.__doc__.split('\n')[0]
        args = spec[spec.find('(') + 1:spec.find(')')]
        return args.count(',') + 1 if args else 0


class Functionality:
    def __init__(self):
        self.p = self.PartialOp()

    @staticmethod
    def curry(f, *arg, **kwarg):
        def curriedv2(*args2, **kwargs2):
            if len(args2) >= num_args(f):
                return f(*args2, **kwargs2)
            else:
                return curried(*args2, **kwargs2)

        def curried(*args, **kwargs):
            re = lambda *a, **kwa: curriedv2(*(args + a), **{**kwargs, **kwa})
            re.c = lambda *args3, **kwargs3: curried(*(args + args3), **{**kwargs, **kwargs3})
            return re

        return curried()

    def __call__(self, f: Union[Callable]) -> Callable:
        if isinstance(f, Callable):
            return _.curry(f)

    def __mod__(self, f: [Callable]) -> Callable:
        return _(f)

    def deconstruct(self, v):
        t = type(v)
        if t == list:
            if v == []:
                return []
            else:
                return head(v), tail(v)
        elif t in (int, float, complex):
            return v
        elif t == tuple:
            return v
        elif t == dict:
            v = copy.copy(v)
            x = list(v.keys())[0]
            return (x, v.pop(x)), v


    class PartialOp:

        def __add__(self, other):
            return _ % getattr(other, '__r' + self.__add__.__name__[2:])

        def __sub__(self, other):
            return _ % getattr(other, '__r' + self.__sub__.__name__[2:])

        def __mul__(self, other):
            return _ % getattr(other, '__r' + self.__mul__.__name__[2:])

        def __matmul__(self, other):
            return _ % getattr(other, '__r' + self.__matmul__.__name__[2:])

        def __mod__(self, other):
            return _ % getattr(other, '__r' + self.__mod__.__name__[2:])

        def __lshift__(self, other):
            return _ % getattr(other, '__r' + self.__lshift__.__name__[2:])

        def __rshift___(self, other):
            return _ % getattr(other, '__r' + self.__rshift__.__name__[2:])

        def __floordiv__(self, other):
            return _ % getattr(other, '__r' + self.__floordiv__.__name__[2:])

        def __pow__(self, other):
            return _ % getattr(other, '__r' + self.__pow__.__name__[2:])

        def __lt__(self, other):
            return _ % getattr(other, self.__lt__.__name__[:])

        def __le__(self, other):
            return _ % getattr(other, self.__le__.__name__[:])

        def __eq__(self, other):
            return _ % getattr(other, self.__eq__.__name__[:])

        def __and__(self, other):
            return _ % getattr(other, '__r' + self.__and__.__name__[2:])

        def __or__(self, other):
            return _ % getattr(other, '__r' + self.__or__.__name__[2:])

        def __xor__(self, other):
            return _ % getattr(other, '__r' + self.__xor__.__name__[2:])

        def __ne__(self, other):
            return _ % getattr(other, self.__ne__.__name__)

        def __gt__(self, other):
            return _ % getattr(other, self.__gt__.__name__[:])

        def __ge__(self, other):
            return _ % getattr(other, self.__ge__.__name__[:])

        def __getitem__(self, other):
            return _ % getattr(other, self.__getitem__.__name__)

        def __truediv__(self, x):
            if isinstance(x, int):
                x = float(x)
            return _ % x.__rtruediv__

        def __radd__(self, other): return _ % getattr(other, '__' + self.__radd__.__name__[3:])

        def __rsub__(self, other): return _ % getattr(other, '__' + self.__rsub__.__name__[3:])

        def __rmul__(self, other): return _ % getattr(other, '__' + self.__rmul__.__name__[3:])

        def __rmatmul__(self, other): return _ % getattr(other, '__' + self.__rmatmul__.__name__[3:])

        def __rmod__(self, other): return _ % getattr(other, '__' + self.__rmod__.__name__[3:])

        def __rlshift__(self, other): return _ % getattr(other, '__' + self.__rlshift__.__name__[3:])

        def __rrshift__(self, other): return _ % getattr(other, '__' + self.__rrshift__.__name__[3:])

        def __rfloordiv__(self, other): return _ % getattr(other, '__' + self.__rfloordiv__.__name__[3:])

        def __rpow__(self, other): return _ % getattr(other, '__' + self.__rpow__.__name__[3:])

        def __rand__(self, other): return _ % getattr(other, '__' + self.__rand__.__name__[3:])

        def __ror__(self, other): return _ % getattr(other, '__' + self.__ror__.__name__[3:])

        def __rxor__(self, other): return _ % getattr(other, '__' + self.__rxor__.__name__[3:])

        def __neg__(self): return lambda x: -x

        def __pos__(self): return lambda x: +x

        def __invert__(self): return lambda x: ~x


_ = Functionality()


class Functor(ABC, Generic[a]):
    @abstractmethod
    def fmap(self, f: Callable[[a], b]) -> 'Functor[a]':
        pass

    @_
    def replace(self, x: b) -> 'Functor[b]':
        return self.fmap(const(x))


class Applicative(Functor, Generic[a]):
    @_
    def __init__(self, x: a):
        self.value = x

    pure = __init__

    @abstractmethod
    def apply(self, f: 'Applicative[Callable[[a], b]]') -> 'Applicative[b]':
        pass

    @_
    def insert(self, x: 'Applicative[b]') -> 'Applicative[b]':
        return self.apply(Applicative(lambda _: x))

    @abstractmethod
    def lift_a2(self, f: Callable[[a, b], c], x: 'Functor[a]', y: 'Functor[b]', z: 'Functor[c]'):
        return self.fmap(f, self.apply(x, y))


class Monad(Applicative, Generic[a]):
    @_
    def inject(self, x: a):
        return super(Applicative).pure(x)

    @abstractmethod
    def apply_m(self, f: Callable[[a], 'Monad[b]']) -> 'Monad[b]':
        pass

    @_
    def insert_m(self, x: 'Monad[b]') -> 'Monad[b]':
        return self.apply_m(lambda _: x)


class Pair(Generic[a, b]):
    @_
    def __init__(self, x: a, y: b):
        self.x = x
        self.y = y

    @_
    def fst(self) -> a: return self.x

    @_
    def snd(self) -> b: return self.y


class Triplet(Generic[a, b, c], Pair):
    @_
    def __init__(self, x: a, y: b, z: c):
        super(Pair).__init__(x, y)
        self.z = z

    @_
    def trd(self) -> c: return self.z


@_
def const(x: a, __: Any) -> Callable[[], a]:
    return lambda: x


@_
def as_type_of(x: a, __: a) -> Callable[[], a]:
    return lambda: x


@_
def comp(*fs: Tuple[Callable]) -> Callable:
    f = lambda *x: head(fs)(head(tail(fs)(*x)))
    if len(fs) == 2:
        return _ % f
    else:
        return comp(f, *tail(tail(fs)))


@_
def flip(f: Callable[[a, b], Any]) -> Callable[[b, a], Any]:
    return lambda a, b: f(b, a)


@_
def identity(x: a) -> a:
    return x


@_
def head(l: List[a]) -> a:
    return l[0]


@_
def tail(l: List[a]) -> List[a]:
    return l[1:]


@_
def last(l: List[a]) -> a:
    return l[-1]


@_
def init(l: List[a]) -> List[a]:
    return l[:-1]


@_
def until(p: Callable[[a], bool], f: Callable[[a], a], x: a) -> a:
    if p(x):
        return until(p, f, f(x))
    else:
        return x


@_
def null(xs: List[a]) -> bool:
    return xs == []


@_
def allp(f: Callable[[a], bool], xs: List[a]) -> bool:
    return all(map(f, xs))


@_
def anyp(f: Callable[[a], bool], xs: List[a]) -> bool:
    return any(map(f, xs))


@_
def scanl(f: Callable[[b, a], b], z: b, xs: List[a]):
    res = [z]
    acc = z
    for x in xs:
        acc = f(acc, x)
        res += [acc]
    return res


@_
def lookup(x: a, xys: Union[Dict[a, b], List[Pair[a, b]]]) -> b:
    if isinstance(xys, dict):
        return xys[x]
    else:
        raise NotImplementedError


take: Callable[[int, List[a]], List[a]] = _ % (lambda n, xs: xs[:n])
drop: Callable[[int, List[a]], List[a]] = _ % (lambda n, xs: xs[n:])

split_at: Callable[[int, List[a]], Pair[List[a], List[a]]] = _ % (lambda n, xs: Pair(take(n, xs), drop(n, xs)))

replicate: Callable[[int, a], List[a]] = _ % (lambda n, x: [x for __ in range(n)])

scanl1: Callable[[Callable[[a, a], a], List[a]], List[a]] = _ % (lambda f, x: list(itertools.accumulate(x, f)))

reverse = _ % reversed

andp = _ % all
orp = _ % any

concat: Callable[[List[List[a]]], List[a]] = _ % (lambda xs: [item for sublist in xs for item in sublist])
map: Callable[[Callable[[a], b], List[a]], List[b]] = _ % (lambda f, *xs: list(builtins.map(f, *xs)))
concat_map = _ % comp(concat, map)

length = _ % len

take_while: Callable[[Callable[[a], bool], List[a]], List[a]] = _ % (lambda p, xs: [] if xs == [] or (not p(head(xs)))
else [head(xs)] + take_while(p, tail(xs)))

drop_while: Callable[[Callable[[a], bool], List[a]], List[a]] = _ % (lambda p, xs: take_while(lambda x: not p(x), xs))

span: Callable[[Callable[[a], bool], List[a]], Pair[List[a], List[a]]] = \
    _ % (lambda p, xs: Pair(take_while(p, xs), drop_while(p, xs)))
breakp: Callable[[Callable[[a], bool], List[a]], Pair[List[a], List[a]]] = _ % (
    lambda p, xs: span(lambda x: not p(x), xs))

elem: Callable[[a, List[a]], bool] = _ % (lambda x, xs: x in xs)
not_elem: Callable[[a, List[a]], bool] = _ % (lambda x, xs: x not in xs)

zip3 = _ % (lambda xs, ys, zs: zip(xs, ys, zs))

zip_with = _ % (lambda f, *xss: map(f, *xss))
zip_with3 = _ % (lambda f, xs, ys, zs: map(f, xs, ys, zs))

unzip = _ % (lambda *l: zip(*l))
unzip3 = _ % (lambda xs, ys, zs: zip(xs, ys, zs))

lines = _ % str.splitlines
words = _ % str.split
unlines = _ % "\n".join
unwords = _ % " ".join




# TODO: scanr, scanr1, iterate, repeat, cycle


